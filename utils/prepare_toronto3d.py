import numpy as np
from pathlib import Path
from tqdm import tqdm

from ply import write_ply, read_ply

output_type = 'npy'
i_dont_have_a_lot_of_memory_ok = True
ROOT_PATH = Path("../../RandLA-Net/data")
DATASET_PATH = ROOT_PATH / 'Toronto_3D'
RAW_PATH = DATASET_PATH / 'original_ply'
TRAIN_PATH = DATASET_PATH / 'train'
VAL_PATH = DATASET_PATH / 'val'

for folder in [TRAIN_PATH, VAL_PATH]:
    folder.mkdir(exist_ok=True)

print(f'Computing point clouds as {output_type} files. This operation is very time-consuming.')

class_counts = np.zeros((9,), dtype=np.uint64); # counts the number of points in each class

for pc_path in RAW_PATH.glob('*.ply'):
    name = pc_path.stem

    if output_type=='ply' :
        pc_name = name + '.ply'
    elif output_type=='npy':
        pc_name = name + '.npy'
    else :
        raise 'unknown output_type'

    # Check if the point cloud has already been computed
    if list(TRAIN_PATH.rglob(pc_name)) != []:
        continue

    print(f'Writing {pc_name}...')

    UTM_OFFSET = [627285, 4841948, 0]
    try:
        ply = read_ply(pc_path)
        points = np.vstack((ply['x'] - UTM_OFFSET[0], ply['y'] - UTM_OFFSET[1], ply['z'] - UTM_OFFSET[2], ply['red'], ply['green'], ply['blue'], ply['scalar_Label'])).T
    except:
        print(f'Error in reading {pc_path}')
        continue
    # if name == "L002":
    #     out_dir = VAL_PATH
    # else:
    out_dir = TRAIN_PATH
    np.save(out_dir / pc_name, points)

    # Add the number of points to the class count
    classes = points[:, -1].astype(np.uint8)
    class_counts += np.bincount(classes, minlength=9).astype(np.uint64)
print('Done.')

print('Class counts:')
print(class_counts)
